import { Component, OnInit } from '@angular/core';

class Rick{
  constructor(
      public id: number,
      public name: string,
      public status: string,
      public species: string,
      public type: string,
      public gender: string,
      public origin: RickObject,
      public location: RickObject,
      public image: string,
      public episode: string [],
      public url: string,
      public created: string,
  ){}
}

class RickObject{
  constructor(
      public name: string,
      public url: string,
  ){}
}

interface Origin {
  name: string;
  url: string;
}

interface Location {
  name: string;
  url: string;
}

interface IApiResponse {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: Origin;
  location: Location;
  image: string;
  episode: string[];
  url: string;
  created: string;
}

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss'],
})
export class CharactersComponent implements OnInit {

  rick: Rick;

  constructor() { }

  ngOnInit() {
    this.rick = <Rick>{};
    this.getCharacterData();
  }

  getCharacterData(){
    fetch(`https://rickandmortyapi.com/api/character/1`)
    .then(response => response.json())
    .then((data: IApiResponse) => {
      this.rick.id = data.id;
      this.rick.name = data.name;
      this.rick.status = data.status;
      this.rick.species = data.species;
      this.rick.type = data.type;
      this.rick.gender = data.gender;
      this.rick.origin = data.origin;      
      this.rick.location = data.location;
      this.rick.origin.name = data.origin.name;
      this.rick.origin.url = data.origin.url;
      this.rick.location.name = data.location.name;
      this.rick.location.url = data.location.url;
      this.rick.image = data.image;
      this.rick.episode = data.episode;
      this.rick.url = data.url;
      this.rick.created = data.created;
    });
  }
}